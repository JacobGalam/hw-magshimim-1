#ifndef STACK_H
#define STACK_H


/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	// (//) is the old vereven
	int value;
	stack* before;  // in first element, before will be the last...

	//stack* next; (save memory)
	//before: stack* last;  // I know this is a waste of memory...
} stack;




void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

#endif // STACK_H