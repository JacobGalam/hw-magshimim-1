#include "stack.h"
#include <iostream>

void initStack(stack* s)
{
	s->value = NULL;
	s->before = NULL;
}

void cleanStack(stack* s)
{
	stack* lastNote = s;
	stack* tempPoninter;

	while (lastNote)
	{
		tempPoninter = lastNote->before;

		lastNote->value = NULL;
		lastNote->before = NULL;
		delete lastNote;

		lastNote = tempPoninter;
	}
}

void push(stack* s, unsigned int element)
{
	stack* oldLastNote = s->before;  // get the last element

	// def the new Node
	stack* newNote = new stack;
	newNote->value = element;
	newNote->before = oldLastNote;

	s->before = newNote;  // cashing the last in the first!
}

int pop(stack* s)
{
	int retrunInt = -1;

	stack* lastNote = s->before;
	if (!lastNote)  // if not last elements is fond (the list empty)
	{
		return retrunInt;
	}
	stack* beforeLastNote = lastNote->before;  // take the secend last 


	s->before = beforeLastNote;  // update the cashe, the first note

	retrunInt = lastNote->value;

	// deleting
	lastNote->value = NULL;
	lastNote->before = NULL;
	delete lastNote;
	
	return retrunInt;
}
